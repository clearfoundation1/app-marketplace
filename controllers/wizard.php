<?php

/**
 * Marketplace wizard controller.
 *
 * @category   apps
 * @package    marketplace
 * @subpackage controllers
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2011 ClearCenter
 * @license    http://www.clearcenter.com/app_license ClearCenter license
 * @link       http://www.clearcenter.com/support/documentation/clearos/marketplace/
 */

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\marketplace\Marketplace as Marketplace;

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Marketplace wizard controller.
 *
 * @category   apps
 * @package    marketplace
 * @subpackage controllers
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2011 ClearCenter
 * @license    http://www.clearcenter.com/app_license ClearCenter license
 * @link       http://www.clearcenter.com/support/documentation/clearos/marketplace/
 */

class Wizard extends ClearOS_Controller
{
    /**
     * Marketplace wizard default controller
     *
     * @param String $category category
     *
     * @return view
     */

    function index($category)
    {
        // Load dependencies
        //------------------
        $this->lang->load('marketplace');
        $this->load->library('marketplace/Marketplace');
        $this->load->library('base/OS');
        $this->load->library('marketplace/Cart');

        // Don't want to show this page if we are not in wizard mode
        if (!$this->session->userdata('wizard'))
            redirect('/marketplace');
            
        // Load view data
        //---------------

        try {
            $os_name = $this->os->get_name();
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        $mode = $this->session->userdata('wizard_marketplace_mode');
        
        if (isset($mode) && $mode !== FALSE)
            $data['mode'] = $mode;
        else
            $data['mode'] = 'mode1';

        // If auto-install is present, install and exit wizard or Load view
        //----------

        $file_path_qsf = $this->marketplace->get_autoinstall_file_path();

        if ($file_path_qsf) {

            try {
                // put the qsf file in installer directory and skip for selection view
                $this->marketplace->set_autoinstaller_qsf();
                
                try {
                    $list = $this->cart->get_items();
                } catch (Exception $e) {
                    // do nothing
                }

                // check if cart from autoinstaller is empty then exit the wizard
                if (empty($list)) {

                    redirect('base/wizard/stop');
                }
                 
                // Initiate Yum install
                $this->load->library('base/Yum');
                
                // Check for cart items that do not have an RPM
                $install_list = array();

                foreach ($list as $item) {

                    if (($item->get_pid_bitmask() & Marketplace::PID_MASK_RPM_AVAIL) == Marketplace::PID_MASK_RPM_AVAIL)
                        $install_list[] = preg_replace("/_/", "-", $item->get_id());
                }

                if (empty($install_list)) {
                    
                    $this->cart->clear();

                    // exit the wizard if the installation list is empty
                    redirect('base/wizard/stop');
                }
                
                $this->yum->install($install_list);

                // If yum starts to install OK, delete cart contents
                $this->cart->clear();

                // Clear cache to force fetching new status
                $this->marketplace->delete_cache(NULL, Marketplace::PREFIX);

                // Clear app cache install list
                $this->marketplace->delete_cached_app_install_list();

                // jump page to progress page
                $this->session->set_userdata(array('wizard_redirect' => 'wizard/stop'));
                redirect('/marketplace/progress');
            
            } catch (Exception $e) {

                //$this->page->view_exception($e); // Not a good idea to show Exception here

                redirect('base/wizard/stop');
            }

        } else {

            // Run normal Wizard Selection
            $this->page->view_form('marketplace/wizard_intro', $data, lang('marketplace_marketplace'), array('type' => MY_Page::TYPE_SPOTLIGHT));
        }
    }

    /**
     * Marketplace wizard selection controller
     *
     * @return view
     */

    function selection()
    {
        // Load dependencies
        //------------------

        $this->lang->load('marketplace');
        $this->load->library('marketplace/Marketplace');
        $this->load->library('marketplace/Cart');
        $this->load->helper('number');

        // Don't want to show this page if we are not in wizard mode
        if (!$this->session->userdata('wizard'))
            redirect('/marketplace');

        $mode = $this->session->userdata('wizard_marketplace_mode');
        $category = 'all';
        if ($mode === FALSE) {
            $mode = 'mode1';
            $category = 'cloud';
        } else if ($mode == 'mode2') {
            $category = 'cloud';
        } else if ($mode == 'mode4') {
            // Exit Wizard
            $this->stop();
            return;
        }

        // Load view
        //----------

        $data = array();

        // Note: setting 'new' to 'all' below is handy for testing
        $this->marketplace->set_search_criteria (
            '',
            $category,
            'all',
            'all',
            'new'
        );
        $data['number_of_apps_to_display'] = '0';
        $data['display_format'] = 'tile';
        $data['wizard'] = TRUE;
        $data['mode'] = $mode;
        $data['os_name'] = $this->session->userdata('os_name');

        // Handle form submit
        //-------------------

        if ($this->input->post('reset')) {
            try {
                $this->marketplace->delete_qsf();
                $this->cart->clear();
                redirect('/marketplace/wizard/selection');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }
        $config['upload_path'] = CLEAROS_TEMP_DIR;
        $config['allowed_types'] = 'txt';
        $config['overwrite'] = TRUE;
        $config['file_name'] = Marketplace::FILE_QSF;

        $this->load->library('upload', $config);

        if ($this->input->post('upload')) {
            if (!$this->upload->do_upload('qsf')) {
                $this->page->set_message($this->upload->display_errors());
                redirect('/marketplace/wizard/selection');
                return;
            } else {
                try {
                    $upload = $this->upload->data();
                    $this->marketplace->set_qsf($upload['file_name']);
                    $data['filename'] = $upload['file_name'];
                    $data['size'] = byte_format($this->marketplace->get_qsf_size(), 1);
                    $data['qsf'] = $this->marketplace->get_qsf_info();
                    $data['qsf_ready'] = TRUE;
                } catch (Exception $e) {
                    $this->page->set_message(clearos_exception_message($e), 'warning');
                    $this->marketplace->delete_qsf();
                    redirect('/marketplace/wizard/selection');
                    return;
                }
            }

            $this->session->set_userdata(array('wizard_redirect' => 'marketplace/install'));
        }

        if ($mode == 'mode3')
            $this->page->view_form('marketplace/quick_select', $data, lang('marketplace_marketplace'), array('type' => MY_Page::TYPE_SPOTLIGHT));
        else if ($mode == 'mode1')
            $this->page->view_form('marketplace/novice', $data, lang('marketplace_marketplace'));
        else
            $this->page->view_form('marketplace/category', $data, lang('marketplace_marketplace'));
    }

    /**
     * Wizard exit.
     *
     * @return void
     */

    function stop()
    {
        // Load dependencies
        //------------------

        $this->load->library('marketplace/Marketplace');

        // Reset our Marketplace search so the last category is not displayed by default
        $this->marketplace->reset_search_criteria();

        $this->page->set_message(lang('base_install_wizard_complete'), 'info', lang('base_status'));
        redirect('base/wizard/stop');
        return;

    }

    /**
     * Set wizard mode.
     *
     * @return void
     */

    function set_mode()
    {
        // Load dependencies
        //------------------

        $this->session->set_userdata(array('wizard_redirect' => 'marketplace/wizard/selection'));
        $this->session->set_userdata(array('wizard_marketplace_mode' => $this->input->post('mode')));

        return;
    }

}
